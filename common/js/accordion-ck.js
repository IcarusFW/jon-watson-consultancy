$(document).ready(function() {
    $(".accordion .content").addClass("hidden");
    $(".accordion .js-slidetoggle").addClass("info-expand");
    $(".accordion .js-slidetoggle").click(function() {
        var e = $(this), t = $(this).next(".content");
        t.slideToggle("fast", "swing", function() {
            t.is(":visible") === !0 ? e.toggleClass("info-collapse info-expand") : e.toggleClass("info-expand info-collapse");
        });
    });
});