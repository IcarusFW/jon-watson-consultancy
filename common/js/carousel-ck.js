// code tutorial for carousel plugin at: http://www.barrelny.com/blog/building-a-jquery-slideshow-plugin-from-scratch
function carousel(e, t, n) {
    function c(e) {
        if (e === "next") {
            o = s + 1;
            o > a - 1 && (o = 0);
        } else if (e === "prev") {
            o = s - 1;
            o < 0 && (o = a - 1);
        } else o = e;
        h(s, o);
    }
    function h(e, t) {
        u.eq(e).css("z-index", 3);
        u.eq(t).css({
            "z-index": 2,
            opacity: 1
        });
        if (!f) u.eq(e).animate({
            opacity: 0
        }, l.fadeDur, function() {
            i();
        }); else {
            var n = {};
            n[f + "transition"] = "opacity " + l.fadeDur + "ms";
            n.opacity = 0;
            u.eq(e).css(n);
            var r = setTimeout(function() {
                i();
            }, l.fadeDur);
            function i() {
                u.eq(e).removeAttr("style");
                s = t;
                p();
            }
        }
    }
    function p() {
        i = setTimeout(function() {
            c("next");
        }, l.slideDur);
    }
    function f(e) {
        var t = [ "Webkit", "Moz", "O", "ms" ];
        for (var n = 0; n < t.length; n++) if (t[n] + "Transition" in e.style) return "-" + t[n].toLowerCase() + "-";
        return "transition" in e.style ? "" : !1;
    }
    e[0].faderConfig = {};
    var r = ".slide", i, s, o, u = e.find(r), a = u.length, f = f(e[0]), l = e[0].faderConfig;
    l = {
        slideDur: t,
        fadeDur: n
    };
    u.eq(0).css("opacity", 1);
    s = 0;
    i = setTimeout(function() {
        c("next");
    }, l.slideDur);
}

$(document).ready(function() {
    carousel($("#js-carousel"), 5e3, 800);
});