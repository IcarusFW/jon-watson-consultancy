$(document).ready(function(){
	$('.accordion .content').addClass('hidden');
	$('.accordion .js-slidetoggle').addClass('info-expand');

	$('.accordion .js-slidetoggle').click(function(){
		var parentTarget = $(this); // set clicked button to a var
		var refTarget = $(this).next('.content'); // find closest summary (this parent)
		refTarget.slideToggle('fast', 'swing', function(){
			if (refTarget.is(':visible') === true) {
				parentTarget
					.toggleClass('info-collapse info-expand');
			} else {
				parentTarget
					.toggleClass('info-expand info-collapse');
			}
		});
	});
});