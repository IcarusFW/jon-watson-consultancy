// code tutorial for carousel plugin at: http://www.barrelny.com/blog/building-a-jquery-slideshow-plugin-from-scratch

function carousel($container, slideDur, fadeDur){
    $container[0].faderConfig = {};
    var slideSelector = '.slide',
        slideTimer,
        activeSlide,
        newSlide,
        $slides = $container.find(slideSelector),
        totalSlides = $slides.length,
        prefix = prefix($container[0]),
        config = $container[0].faderConfig;
    config = {
        slideDur : slideDur,
        fadeDur : fadeDur
    };
    $slides.eq(0).css('opacity', 1);
    activeSlide = 0;
    slideTimer = setTimeout(function(){
        changeSlides('next');
    },config.slideDur);
    function changeSlides(target){
        if(target === 'next'){
            newSlide = activeSlide + 1;
            if(newSlide > totalSlides - 1){
                newSlide = 0;
            }
        } else if(target === 'prev'){
            newSlide = activeSlide - 1;
            if(newSlide < 0){
                newSlide = totalSlides - 1;
            }
        } else {
            newSlide = target;
        }
        animateSlides(activeSlide, newSlide);
    }
    function animateSlides(activeNdx, newNdx){
        $slides.eq(activeNdx).css('z-index', 3);
        $slides.eq(newNdx).css({
            'z-index': 2,
            'opacity': 1
        });
        if(!prefix){ // If no transition support, use jQuery
			$slides.eq(activeNdx).animate({'opacity': 0}, config.fadeDur,
			function(){
				cleanUp();
			});
		} else { // Else, use CSS3
			var styles = {};
			styles[prefix+'transition'] = 'opacity '+config.fadeDur+'ms';
			styles.opacity = 0;

			// now pull data from object using .css()
			$slides.eq(activeNdx).css(styles);

			// now wait for animation to end
			var fadeTimer = setTimeout(function(){
				cleanUp();
			},config.fadeDur);
			function cleanUp(){
				$slides.eq(activeNdx).removeAttr('style');
				activeSlide = newNdx;
				waitForNext();
			}
		}
    }
    function waitForNext(){
        slideTimer = setTimeout(function(){
            changeSlides('next');
        },config.slideDur);
    }
    function prefix(el) {
		var prefixes = ["Webkit", "Moz", "O", "ms"];
		for (var i = 0; i < prefixes.length; i++){
			if (prefixes[i] + "Transition" in el.style){
				return '-'+prefixes[i].toLowerCase()+'-';
			}
		}
		return "transition" in el.style ? "" : false;
	}
}

$(document).ready(function(){
	carousel($('#js-carousel'), 5000, 800);
});